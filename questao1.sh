#!/bin/bash

opcao=0

for (( ; ; )); do
  echo "===== Menu ====="
  echo "0. Exibir as 10 primeiras linhas"
  echo "1. Exibir as 10 últimas linhas"
  echo "2. Exibir o número de linhas"
  echo "3. Sair"
  echo "================"

  read -p "Escolha uma opção: " opcao

  [ $opcao -eq 0 ] && head -n 10 "$1"
  [ $opcao -eq 1 ] && tail -n 10 "$1"
  [ $opcao -eq 2 ] && wc -l "$1" 
  [ $opcao -eq 3 ] && break

done
