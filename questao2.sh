#!/bin/bash

sites=("www.google.com" "www.facebook.com" "www.instagram.com" "www.leagueoflegends.com")
sucesso=0
falha=0

for site in "${sites[@]}"; do
  ping -c 1 "$site" >/dev/null && ((sucesso++)) || ((falha++))
done

echo "Quantidade de pings bem-sucedidos: $sucesso"
echo "Quantidade de pings falhados: $falha"
